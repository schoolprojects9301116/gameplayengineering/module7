# RPG Movement

This repository contains projects to test different common 'rpg' mechanics.

## Movement

The first phase of this project was to implement rpg exploration mechanics and to do so using the State Pattern. Creatin an initial base system for the State Pattern and then applying it to easily seperate out actions the player can provide given their current state. 

To really test this approach, I impelement some common open world RPG mechanics including:
    1. Jumping
    2. Climbing
    3. Gliding
    4. Walking

This phase of the project was implemented in 1 week.

## Inventory

The second phase of was to create a inventory crafting system. Choosing how to represent items, inventory space required a lot of thought and iteration. After words, to make the inventory usable to test how effective the system was involved a simple crafting system.

This phase was implemented in 1 week.

# Project Specifications

This project was created using Unreal Engine 5.0
