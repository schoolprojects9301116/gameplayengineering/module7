// Fill out your copyright notice in the Description page of Project Settings.


#include "ClimbPlayerPawnState.h"

#include "PlayerPawn.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "BasePlayerPawnState.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "JumpPlayerPawnState.h"
#include "CollectibleActor.h"

UClimbPlayerPawnState::~UClimbPlayerPawnState() {}

UPlayerPawnState* UClimbPlayerPawnState::HandleAxis(APlayerPawn& pawn, InputAxisType axisType, float inputValue)
{
	bool characterDropped = false;
	if (axisType == InputAxisType::MoveForward)
	{
		characterDropped = ClimbMovement(pawn, pawn.GetActorUpVector(), inputValue);
	}
	else if (axisType == InputAxisType::MoveRight)
	{
		characterDropped = ClimbMovement(pawn, pawn.GetActorRightVector(), inputValue);
	}
	if (characterDropped)
	{
		return NewObject<UJumpPlayerPawnState>(UJumpPlayerPawnState::StaticClass());
	}
	else
	{
		return nullptr;
	}
}

UPlayerPawnState* UClimbPlayerPawnState::HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent)
{
	if (actionType == InputActionType::Climb && inputEvent == IE_Pressed)
	{
		DropFromWall(pawn);
		return NewObject<UJumpPlayerPawnState>(UJumpPlayerPawnState::StaticClass());
	}
	else if (actionType == InputActionType::Interact && inputEvent == IE_Pressed)
	{
		if (pawn.CurrentlyCollectible != nullptr && pawn.CurrentlyCollectible->CollectibleProperties.CollectWhileClimbing)
		{
			pawn.AddToInventory();
		}
	}
	return nullptr;
}

bool UClimbPlayerPawnState::ClimbMovement(APlayerPawn& pawn, FVector direction, float inputValue)
{
	if (inputValue == 0)
	{
		return false;
	}

	auto pawnCapsule = pawn.GetCapsuleComponent();
	float capsuleRadius = pawnCapsule->GetScaledCapsuleRadius();

	FVector startPos = direction * inputValue * capsuleRadius * 2;
	startPos = startPos + pawn.GetActorLocation();
	
	FVector endPos = pawn.GetActorForwardVector();
	endPos *= capsuleRadius * 4;
	endPos += startPos;

	FHitResult hitInfo;
	if (pawn.GetWorld()->LineTraceSingleByChannel(hitInfo, startPos, endPos, ECollisionChannel::ECC_Visibility))
	{
		endPos = pawn.GetActorForwardVector();
		endPos *= 3 * capsuleRadius;
		endPos = startPos + endPos;

		FHitResult wallHitInfo;
		if (pawn.GetWorld()->LineTraceSingleByChannel(wallHitInfo, startPos, endPos, ECollisionChannel::ECC_Visibility))
		{
			auto angleCheck = UKismetMathLibrary::MakeRotFromX(wallHitInfo.Normal * -1).Pitch;
			if (angleCheck > pawn.MaxClimbAngle)
			{
				return false;
			}
			if (angleCheck < pawn.MinClimbAngle)
			{
				DropFromWall(pawn);
				return true;
			}
				
			FVector moveOffset = wallHitInfo.Normal * capsuleRadius;
			moveOffset = wallHitInfo.Location + moveOffset;
			moveOffset = UKismetMathLibrary::GetDirectionUnitVector(pawn.GetActorLocation(), moveOffset);
			moveOffset *= inputValue > 0 ? 1.0f : -1.0f;
			pawn.AddMovementInput(moveOffset, inputValue);
			FVector targetRotVector = hitInfo.Normal * -1;
			FRotator newRotation = FMath::RInterpTo(pawn.GetActorRotation()
					, UKismetMathLibrary::MakeRotFromX(targetRotVector)
				, pawn.GetWorld()->GetDeltaSeconds(), 5.f);
			pawn.SetActorRotation(newRotation);
		}
	}
	else if (direction == pawn.GetActorUpVector() && inputValue > 0)
	{
		DropFromWall(pawn);
		pawn.GetCharacterMovement()->JumpZVelocity *= 1.5;
		pawn.Jump();
		return true;
	}
	return false;
}

void UClimbPlayerPawnState::DropFromWall(APlayerPawn& pawn)
{
	auto movement = pawn.GetCharacterMovement();
	movement->SetMovementMode(EMovementMode::MOVE_Walking);
	movement->bOrientRotationToMovement = true;
	auto actorRotation = pawn.GetActorRotation();
	pawn.SetActorRotation(FRotator(0, actorRotation.Yaw, actorRotation.Roll));
}
