// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PotionStructure.generated.h"

class UTexture2D;

/**
 * 
 */
USTRUCT(BlueprintType)
struct FPotionStructure
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
	FString PotionName;

	UPROPERTY(EditAnywhere)
	FString ModifierName;

	UPROPERTY(EditAnywhere)
	float Modifier;

	UPROPERTY(EditAnywhere)
	UTexture2D * PotionImage;
};
