// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CollectibleStructure.generated.h"

class UTexture2D;

USTRUCT(BlueprintType)
struct FCollectibleStructure
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	FString ItemName;

	UPROPERTY(EditAnywhere)
	bool CollectWhileClimbing;

	UPROPERTY(EditAnywhere)
	UTexture2D * InventoryImage;

	UPROPERTY(EditAnywhere)
	TMap<FString, float> PropertyModifiers;
};