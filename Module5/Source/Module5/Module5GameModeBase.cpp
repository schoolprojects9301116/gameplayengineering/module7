// Copyright Epic Games, Inc. All Rights Reserved.


#include "Module5GameModeBase.h"


AModule5GameModeBase::AModule5GameModeBase()
{
	CraftingSystem = NewObject<UCraftingSystem>(UCraftingSystem::StaticClass());
}

void AModule5GameModeBase::StartPlay()
{
	if (CraftingSystem == nullptr)
	{
		CraftingSystem = NewObject<UCraftingSystem>(UCraftingSystem::StaticClass());
	}
	if (CraftingSystem != nullptr)
	{
		CraftingSystem->PossibleMods = PossibleMods;
		CraftingSystem->RuinedPotion = RuinedPotion;
	}
	Super::StartPlay();
}
