// Fill out your copyright notice in the Description page of Project Settings.


#include "JumpPlayerPawnState.h"

#include "PlayerPawn.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BasePlayerPawnState.h"
#include "GlidePlayerPawnState.h"

#include "Kismet/GameplayStatics.h"

UJumpPlayerPawnState::~UJumpPlayerPawnState()
{}

void UJumpPlayerPawnState::EnterState(APlayerPawn& pawn)
{
	if (pawn.JumpingSound != nullptr && !pawn.GetCharacterMovement()->IsFalling())
	{
		UGameplayStatics::PlaySound2D(pawn.GetWorld(), pawn.JumpingSound, 1, 1, 0.75);
		pawn.Jump();
	}
}

UPlayerPawnState* UJumpPlayerPawnState::HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent)
{
	if (actionType == InputActionType::Jump)
	{
		if (inputEvent == IE_Pressed)
		{
			return NewObject<UGlidePlayerPawnState>(UGlidePlayerPawnState::StaticClass());
		}
		else if (inputEvent == IE_Released)
		{
			pawn.StopJumping();
		}
	}
	return nullptr;
}

UPlayerPawnState* UJumpPlayerPawnState::HandleAxis(APlayerPawn& pawn, InputAxisType axisType, float inputValue)
{
	auto controller = pawn.GetController();
	if (controller != nullptr)
	{
		const FRotator rotation = controller->GetControlRotation();
		const FRotator yawRotation(0, rotation.Yaw, 0);
		FVector direction;
		switch (axisType)
		{
		case InputAxisType::MoveForward:
			//direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::X);
			direction = pawn.GetActorForwardVector();
			break;
		case InputAxisType::MoveRight:
			//direction = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::Y);
			direction = pawn.GetActorRightVector();
			break;
		default:
			break;
		}
		pawn.AddMovementInput(direction, inputValue);
	}
	return nullptr;
}

UPlayerPawnState* UJumpPlayerPawnState::Update(APlayerPawn& pawn, float dt)
{
	auto movement = pawn.GetCharacterMovement();
	if (movement != nullptr)
	{
		if (!movement->IsFalling())
		{
			movement->GravityScale = 1.f;
			return NewObject<UBasePlayerPawnState>(UBasePlayerPawnState::StaticClass());
		}
		else if (pawn.GetVelocity().Z <= 0.f)
		{
			movement->GravityScale += 5 * dt;
		}
	}
	return nullptr;
}
