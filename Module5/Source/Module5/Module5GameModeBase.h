// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CraftingSystem.h"

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Module5GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MODULE5_API AModule5GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	AModule5GameModeBase();

	UPROPERTY(EditAnywhere)
	UCraftingSystem * CraftingSystem;

	UPROPERTY(EditAnywhere)
	FPotionStructure RuinedPotion;

	virtual void StartPlay() override;

private:
	UPROPERTY(EditAnywhere)
	TMap<FString, UTexture2D*> PossibleMods;
};
