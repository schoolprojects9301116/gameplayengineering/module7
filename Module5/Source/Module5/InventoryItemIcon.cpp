// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItemIcon.h"

#include "Engine/Texture2D.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"

void UInventoryItemIcon::SetCount(int count)
{
	currentItemCount = count;
	ItemCount->SetText(FText::FromString(FString::Printf(TEXT("x%d"), count)));
}

void UInventoryItemIcon::SetImage(UTexture2D* img)
{
	ItemImage->SetBrushFromTexture(img);
}

void UInventoryItemIcon::SetupItemIcon(FInventoryItemStructure itemStruct)
{
	ItemStructure = itemStruct;
	SetImage(itemStruct.CollectibleInfo.InventoryImage);
	SetCount(itemStruct.Count);
	ItemButton->OnClicked.AddUniqueDynamic(this, &UInventoryItemIcon::ButtonPressed);
}

void UInventoryItemIcon::ButtonPressed()
{
	if (currentItemCount > 0)
	{
		SetCount(currentItemCount - 1);
		ItemSelected.Broadcast(ItemStructure);
	}
}

void UInventoryItemIcon::ResetIcon()
{
	SetupItemIcon(ItemStructure);
}
