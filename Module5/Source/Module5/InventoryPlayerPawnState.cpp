// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryPlayerPawnState.h"
#include "Module5GameModeBase.h"

#include "Components/CanvasPanel.h"
#include "BasePlayerPawnState.h"
#include "InventoryView.h"
#include "PlayerPawn.h"
#include "Components/Button.h"


void UInventoryPlayerPawnState::EnterState(APlayerPawn& pawn)
{
	inventoryWidget = CreateWidget<UInventoryView>(pawn.GetWorld(), pawn.InventoryView);
	if (inventoryWidget != nullptr)
	{
		inventoryWidget->CraftButton->OnClicked.AddDynamic(this, &UInventoryPlayerPawnState::CraftButtonPressed);
		inventoryWidget->CancelButton->OnClicked.AddDynamic(this, &UInventoryPlayerPawnState::CancelButtonPressed);
		auto controller = Cast<APlayerController>(pawn.GetController());
		if (controller)
		{
			controller->bShowMouseCursor = true;
			inventoryWidget->bIsFocusable = false;
			inventoryWidget->SetAnchorsInViewport(FAnchors(0.5, 0.5));
			inventoryWidget->SetAlignmentInViewport(FVector2D(0.5, 0.5));
			inventoryWidget->SetInventory(pawn.GetInventory());
			inventoryWidget->AddToViewport(999);
			inventoryWidget->SetVisibility(ESlateVisibility::Visible);
			controller->SetInputMode(FInputModeGameAndUI());
		}
		inventoryWidget->InventoryItemSelected.AddDynamic(this, &UInventoryPlayerPawnState::ItemButtonPressed);
	}
}

void UInventoryPlayerPawnState::ExitState(APlayerPawn& pawn)
{
	if (inventoryWidget != nullptr)
	{
		inventoryWidget->RemoveFromViewport();
	}
	auto controller = Cast<APlayerController>(pawn.GetController());
	if (controller)
	{
		controller->bShowMouseCursor = false;
	}
}

UPlayerPawnState* UInventoryPlayerPawnState::HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent)
{
	if (actionType == InputActionType::Inventory && inputEvent == IE_Pressed)
	{
		return NewObject<UBasePlayerPawnState>(UBasePlayerPawnState::StaticClass());
	}
	return nullptr;
}

void UInventoryPlayerPawnState::CancelButtonPressed()
{
	SelectedItems.Empty();
	if (inventoryWidget != nullptr)
	{
		inventoryWidget->ClearCraftItems();
	}
}

void UInventoryPlayerPawnState::CraftButtonPressed()
{
	craftButtonPressed = true;
}

void UInventoryPlayerPawnState::ItemButtonPressed(FInventoryItemStructure itemSelected)
{
	if(inventoryWidget != nullptr)
	{
		if (SelectedItems.Num() < 3)
		{
			SelectedItems.Add(itemSelected);
			inventoryWidget->SetCraftItem(itemSelected, SelectedItems.Num());
		}
	}
}

UPlayerPawnState* UInventoryPlayerPawnState::Update(APlayerPawn& pawn, float dt)
{
	if (craftButtonPressed)
	{
		TArray<FCollectibleStructure> collectibleInfo;
		for (auto& item : SelectedItems)
		{
			collectibleInfo.Add(item.CollectibleInfo);
			pawn.RemoveOneItemFromInventory(item.CollectibleInfo.ItemName);
		}
		auto gameMode = Cast<AModule5GameModeBase>(pawn.GetWorld()->GetAuthGameMode());
		if (gameMode != nullptr)
		{
			auto potion = gameMode->CraftingSystem->CreatePotion(collectibleInfo);
			pawn.AddPotion(potion);
			return NewObject<UBasePlayerPawnState>(UBasePlayerPawnState::StaticClass());
		}
	}
	return nullptr;
}
