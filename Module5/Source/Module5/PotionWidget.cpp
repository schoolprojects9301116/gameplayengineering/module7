// Fill out your copyright notice in the Description page of Project Settings.


#include "PotionWidget.h"

#include "Components/ProgressBar.h"

void UPotionWidget::UpdateProgressBar(float currentTime, float maxTime)
{
	if (currentTime >= 0 && maxTime > 0)
	{
		ActivePotionTimer->SetPercent(currentTime / maxTime);
	}
}