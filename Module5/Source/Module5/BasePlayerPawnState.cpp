// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePlayerPawnState.h"

#include "PlayerPawn.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "JumpPlayerPawnState.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "ClimbPlayerPawnState.h"
#include "InventoryPlayerPawnState.h"

#include "Kismet/GameplayStatics.h"

UBasePlayerPawnState::~UBasePlayerPawnState()
{}

void UBasePlayerPawnState::EnterState(APlayerPawn& pawn)
{
	if (pawn.LandingSound != nullptr)
	{
		UGameplayStatics::PlaySound2D(pawn.GetWorld(), pawn.LandingSound, 1, 1);
	}
}

void UBasePlayerPawnState::ExitState(APlayerPawn& pawn)
{

}

UPlayerPawnState* UBasePlayerPawnState::HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent)
{
	GEngine->AddOnScreenDebugMessage(0, 1.f, FColor::Yellow, TEXT("Inventory HandleAction"));
	switch (actionType)
	{
	case InputActionType::Attack:
		break;
	case InputActionType::Interact:
		if (pawn.CurrentlyCollectible != nullptr)
		{
			pawn.AddToInventory();
		}
		break;
	case InputActionType::Inventory:
		GEngine->AddOnScreenDebugMessage(0, 1.f, FColor::Yellow, TEXT("Inventory Base State"));
		if (inputEvent == IE_Pressed)
		{
			return NewObject<UInventoryPlayerPawnState>(UInventoryPlayerPawnState::StaticClass());
		}
		break;
	case InputActionType::Jump:
		if (inputEvent == IE_Pressed)
		{
			return NewObject<UJumpPlayerPawnState>(UJumpPlayerPawnState::StaticClass());
		}
		break;
	case InputActionType::Climb:
		if (AttemptToGrabWall(pawn))
		{
			return NewObject<UClimbPlayerPawnState>(UClimbPlayerPawnState::StaticClass());
		}
		break;

	case InputActionType::Potion:
		pawn.UsePotion();
		break;
	default:
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("HandleAction IMPOSSIBLE ACTION"));
		break;
	}
	return nullptr;
}

UPlayerPawnState* UBasePlayerPawnState::HandleAxis(APlayerPawn& pawn, InputAxisType axisType, float inputValue)
{
	const FRotator yawRotation(0, pawn.GetControlRotation().Yaw, 0);
	FVector direction;
	switch (axisType)
	{
	case InputAxisType::MoveForward:
		//direction = FRotationMatrix(yawRotation).GetScaledAxis(EAxis::X);
		direction = pawn.GetActorForwardVector();
		break;
	case InputAxisType::MoveRight:
		direction = pawn.GetActorRightVector();
		//direction = FRotationMatrix(yawRotation).GetScaledAxis(EAxis::Y);
		break;
	default:
		break;
	}
	pawn.AddMovementInput(direction, inputValue);
	return nullptr;
}

UPlayerPawnState* UBasePlayerPawnState::Update(APlayerPawn& pawn, float dt)
{
	auto characterMovement = pawn.GetCharacterMovement();
	if (characterMovement->IsFalling())
	{
		return NewObject<UJumpPlayerPawnState>(UJumpPlayerPawnState::StaticClass());
	}
	return nullptr;
}

bool UBasePlayerPawnState::AttemptToGrabWall(APlayerPawn& pawn)
{
	auto pawnCapsule = pawn.GetCapsuleComponent();
	FVector startPos = pawnCapsule->GetComponentLocation();
	FVector endPos = pawn.GetActorForwardVector();
	endPos *= 3 * pawnCapsule->GetScaledCapsuleRadius();
	endPos = startPos + endPos;
	FHitResult hitInfo;
	if (pawn.GetWorld()->LineTraceSingleByChannel(hitInfo, startPos, endPos, ECollisionChannel::ECC_Visibility))
	{
		auto movement = pawn.GetCharacterMovement();
		movement->SetMovementMode(EMovementMode::MOVE_Flying);
		movement->StopMovementImmediately();
		movement->bOrientRotationToMovement = false;

		FVector offset = hitInfo.Normal *pawnCapsule->GetScaledCapsuleRadius();
		offset += hitInfo.Location;

		FRotator rotationOffset = UKismetMathLibrary::MakeRotFromX(hitInfo.Normal * -1);
		pawnCapsule->SetWorldLocationAndRotation(offset, rotationOffset, true);//.MoveComponent(offset, rotationOffset, true);
		//pawn.GetC
		return true;
	}
	return false;
}
