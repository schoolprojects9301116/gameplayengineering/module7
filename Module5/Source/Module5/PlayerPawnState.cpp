// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnState.h"

UPlayerPawnState::UPlayerPawnState() {}

UPlayerPawnState::~UPlayerPawnState() {}

void UPlayerPawnState::EnterState(APlayerPawn& pawn)
{}

void UPlayerPawnState::ExitState(APlayerPawn& pawn)
{}

UPlayerPawnState* UPlayerPawnState::HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent)
{
	return nullptr;
}

UPlayerPawnState* UPlayerPawnState::HandleAxis(APlayerPawn& pawn, InputAxisType axisType, float inputValue)
{
	return nullptr;
}

UPlayerPawnState* UPlayerPawnState::Update(APlayerPawn& pawn, float dt)
{
	return nullptr;
}
