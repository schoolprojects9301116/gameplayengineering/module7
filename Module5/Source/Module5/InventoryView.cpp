// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryView.h"
#include "Components/Image.h"

#include "Components/WrapBox.h"
#include "InventoryItemIcon.h"
#include "Components/Button.h"

void UInventoryView::SetInventory(TMap<FString, FInventoryItemStructure> inventory)
{
	InventoryGrid->ClearChildren();
	for (auto& item : inventory)
	{
		auto properties = item.Value;
		auto itemIcon = CreateWidget<UInventoryItemIcon>(this, itemIconType);
		itemIcon->SetupItemIcon(properties);
		itemIcon->ItemSelected.AddDynamic(this, &UInventoryView::ButtonPressed);
		InventoryGrid->AddChildToWrapBox(itemIcon);
	}
}

void UInventoryView::ButtonPressed(FInventoryItemStructure itemSelected)
{
	GEngine->AddOnScreenDebugMessage(0, 1.f, FColor::Green, itemSelected.CollectibleInfo.ItemName);
	InventoryItemSelected.Broadcast(itemSelected);
}

void UInventoryView::SetCraftItem(FInventoryItemStructure item, int craftSlot)
{
	switch (craftSlot)
	{
	case 1:
		Item1->SetBrushFromTexture(item.CollectibleInfo.InventoryImage);
		break;
	case 2:
		Item2->SetBrushFromTexture(item.CollectibleInfo.InventoryImage);
		break;
	case 3:
		Item3->SetBrushFromTexture(item.CollectibleInfo.InventoryImage);
		break;
	default:
		break;
	}
}

void UInventoryView::ClearCraftItems()
{
	Item1->SetBrushFromTexture(DefaultImg);
	Item2->SetBrushFromTexture(DefaultImg);
	Item3->SetBrushFromTexture(DefaultImg);

	for (auto& child : InventoryGrid->GetAllChildren())
	{
		auto ItemIcon = Cast<UInventoryItemIcon>(child);
		if (ItemIcon != nullptr)
		{
			ItemIcon->ResetIcon();
		}
	}
}
