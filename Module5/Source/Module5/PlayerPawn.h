// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PotionStructure.h"
#include "PlayerPawnState.h"
#include "CollectibleStructure.h"
#include "InventoryItemStructure.h"
#include "InventoryView.h"
#include "PotionWidget.h"

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerPawn.generated.h"

class ACollectibleActor;

UCLASS()
class MODULE5_API APlayerPawn : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerPawn();

	UPROPERTY(EditAnywhere)
	ACollectibleActor* CurrentlyCollectible = nullptr;

	UPROPERTY(EditAnywhere)
	float MinClimbAngle;

	UPROPERTY(EditAnywhere)
	float MaxClimbAngle;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* GliderMesh;

	UPROPERTY(EditAnywhere, Category="Sounds")
	USoundBase* GliderSound;

	UPROPERTY(EditAnywhere, Category = "Sounds")
	USoundBase* JumpingSound;

	UPROPERTY(EditAnywhere, Category = "Sounds")
	USoundBase* LandingSound;

	UPROPERTY(EditAnywhere, Category = "Sounds")
	USoundBase* ClimbingSound;

	UPROPERTY(EditAnywhere, Category = "Sounds")
	USoundBase* WalkingSound;

	UFUNCTION()
	void AddToInventory();

	UFUNCTION(BlueprintPure)
	TMap<FString, FInventoryItemStructure> GetInventory() const;

	UFUNCTION()
	void RemoveOneItemFromInventory(FString itenName);

	UPROPERTY(EditAnywhere)
	TSubclassOf<UInventoryView> InventoryView;

	UFUNCTION()
	void AddPotion(FPotionStructure potion);

	UFUNCTION()
	void UsePotion();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	DECLARE_DELEGATE_TwoParams(ForwardInputActionDelegate, InputActionType, EInputEvent);


	UPROPERTY(EditAnywhere, Category="Player Input")
	TSubclassOf<UPlayerPawnState> startingState;

	UPROPERTY()
	UPlayerPawnState* currentState;

	UFUNCTION()
	void ForwardMoveForward(float axisValue);

	UFUNCTION()
	void ForwardMoveRight(float axisValue);

	UFUNCTION()
	void ForwardInputAction(InputActionType actionType, EInputEvent inputEvent);

	UFUNCTION()
	void ForwardInputAxis(InputAxisType axisType, float axisValue);


	UFUNCTION()
	void ApplyPotionMods();

	UFUNCTION()
	void UndoPotionMods();

	UPROPERTY()
	TMap<FString, FInventoryItemStructure> Inventory;

	UPROPERTY()
	bool HasPotion;

	UPROPERTY()
	bool HasActivePotion;

	UPROPERTY()
	FPotionStructure ActivePotion;

	UPROPERTY()
	FPotionStructure CurrentPotion;

	UPROPERTY()
	float PotionDuration = -1;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UPotionWidget> potionWidgetType;

	UPROPERTY()
	UPotionWidget * potionWidget;

	UPROPERTY(EditAnywhere)
	float maxPotionDuration;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
