// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PotionStructure.h"
#include "CollectibleStructure.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CraftingSystem.generated.h"

class UTexture2D;

/**
 * 
 */
UCLASS()
class MODULE5_API UCraftingSystem : public UObject
{
	GENERATED_BODY()

public:

	FPotionStructure CreatePotion(TArray<FCollectibleStructure> collectibleInfo);

protected:

	UPROPERTY(EditAnywhere)
	FPotionStructure RuinedPotion;

	UPROPERTY(EditAnywhere)
	TMap<FString, UTexture2D*> PossibleMods;

	friend class AModule5GameModeBase;
	
};
