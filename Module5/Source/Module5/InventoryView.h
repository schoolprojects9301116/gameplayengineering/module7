// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InventoryItemStructure.h"
#include "InventoryItemIcon.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InventoryView.generated.h"

class UWrapBox;
class UInventoryItemIcon;

/**
 * 
 */
UCLASS()
class MODULE5_API UInventoryView : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable)
	void SetInventory(TMap<FString, FInventoryItemStructure> inventory);

	UPROPERTY()
	FInventoryItemSelectedSignature InventoryItemSelected;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton * CancelButton;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton * CraftButton;

	UFUNCTION()
	void SetCraftItem(FInventoryItemStructure item, int craftSlot);

	UFUNCTION()
	void ClearCraftItems();

protected:

	DECLARE_DELEGATE_OneParam(FButtonPressedDelegate, FInventoryItemStructure)

	UFUNCTION()
	void ButtonPressed(FInventoryItemStructure itemSelected);

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage * Item1;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage * Item2;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage * Item3;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UWrapBox * InventoryGrid;

	UPROPERTY(EditAnywhere)
	UTexture2D* DefaultImg;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UInventoryItemIcon> itemIconType;
};
