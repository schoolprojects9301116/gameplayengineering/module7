// Fill out your copyright notice in the Description page of Project Settings.


#include "GlidePlayerPawnState.h"
#include "PlayerPawn.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BasePlayerPawnState.h"
#include "Kismet/GameplayStatics.h"

UGlidePlayerPawnState::~UGlidePlayerPawnState()
{}

UPlayerPawnState* UGlidePlayerPawnState::HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent)
{
	if (actionType == InputActionType::Jump)
	{
		if (inputEvent == IE_Pressed)
		{
			return NewObject<UJumpPlayerPawnState>(UJumpPlayerPawnState::StaticClass());
		}
	}
	return nullptr;
}

void UGlidePlayerPawnState::EnterState(APlayerPawn& pawn)
{
	auto characterMovement = pawn.GetCharacterMovement();
	characterMovement->GravityScale = 0.1f;
	characterMovement->AirControl = 1.f;
	pawn.GetVelocity();
	characterMovement->Velocity.Z = 0;

	if (pawn.GliderMesh != nullptr)
	{
		pawn.GliderMesh->SetHiddenInGame(false);
	}
	if (pawn.GliderSound != nullptr)
	{
		UGameplayStatics::PlaySound2D(pawn.GetWorld(), pawn.GliderSound, 1, 1, 0.25);
	}
}

void UGlidePlayerPawnState::ExitState(APlayerPawn& pawn)
{
	auto characterMovement = pawn.GetCharacterMovement();
	characterMovement->GravityScale = 1.f;
	characterMovement->AirControl = 0.5f;
	characterMovement->JumpZVelocity = 420;
	if (pawn.GliderMesh != nullptr)
	{
		pawn.GliderMesh->SetHiddenInGame(true);
	}
}

UPlayerPawnState* UGlidePlayerPawnState::Update(APlayerPawn& pawn, float dt)
{
	auto movement = pawn.GetCharacterMovement();
	if (movement != nullptr)
	{
		if (!movement->IsFalling())
		{
			return NewObject<UBasePlayerPawnState>(UBasePlayerPawnState::StaticClass());
		}
	}
	return nullptr;
}