// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CollectibleStructure.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CollectibleActor.generated.h"

class UBoxComponent;
class UWidgetComponent;

UCLASS()
class MODULE5_API ACollectibleActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACollectibleActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent * StaticMesh;

	UPROPERTY(EditAnywhere)
	UBoxComponent * BoxCollider;

	UPROPERTY(EditAnywhere)
	UWidgetComponent * InteractionPromptWidget;

public:	

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
			, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep
		, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
		, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FCollectibleStructure CollectibleProperties;

};
