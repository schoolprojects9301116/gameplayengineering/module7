// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PotionWidget.generated.h"

class UImage;
class UProgressBar;

/**
 * 
 */
UCLASS()
class MODULE5_API UPotionWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage* ActivePotionImage;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage* HeldPotionImage;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UProgressBar* ActivePotionTimer;

	void UpdateProgressBar(float currentTime, float maxTime);
};
