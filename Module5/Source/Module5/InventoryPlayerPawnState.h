// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InventoryItemIcon.h"

#include "CoreMinimal.h"
#include "PlayerPawnState.h"
#include "InventoryPlayerPawnState.generated.h"

class UCanvasPanel;
class UInventoryView;

/**
 * 
 */
UCLASS()
class MODULE5_API UInventoryPlayerPawnState : public UPlayerPawnState
{
	GENERATED_BODY()

public:
	virtual void EnterState(APlayerPawn& pawn);
	virtual void ExitState(APlayerPawn& pawn);
	virtual UPlayerPawnState* HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent) override;
	virtual UPlayerPawnState* Update(APlayerPawn& pawn, float dt) override;

protected:
	UFUNCTION()
	void ItemButtonPressed(FInventoryItemStructure itemSelected);

	UFUNCTION()
	void CraftButtonPressed();

	UFUNCTION()
	void CancelButtonPressed();

private:

	bool craftButtonPressed = false;

	UPROPERTY()
	UInventoryView* inventoryWidget;

	UPROPERTY()
	TArray<FInventoryItemStructure> SelectedItems;
};
