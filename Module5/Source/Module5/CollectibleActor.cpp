// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectibleActor.h"

#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"

#include "PlayerPawn.h"

// Sets default values
ACollectibleActor::ACollectibleActor()
{
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	SetRootComponent(StaticMesh);

	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	BoxCollider->SetGenerateOverlapEvents(true);
	BoxCollider->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	BoxCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	BoxCollider->SetupAttachment(StaticMesh);

	InteractionPromptWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("InteractionPromptWidget"));
	InteractionPromptWidget->SetWidgetSpace(EWidgetSpace::Screen);
	InteractionPromptWidget->SetHiddenInGame(true);

	InteractionPromptWidget->SetupAttachment(BoxCollider);
}

// Called when the game starts or when spawned
void ACollectibleActor::BeginPlay()
{
	Super::BeginPlay();
	
	BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &ACollectibleActor::OnOverlapBegin);
	BoxCollider->OnComponentEndOverlap.AddDynamic(this, &ACollectibleActor::OnOverlapEnd);
}

void ACollectibleActor::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
		, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep
	, const FHitResult& SweepResult)
{
	auto PlayerActor = Cast<APlayerPawn>(OtherActor);
	if (PlayerActor != nullptr)
	{
		InteractionPromptWidget->SetHiddenInGame(false);
		PlayerActor->CurrentlyCollectible = this;
	}
}

void ACollectibleActor::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
	, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	auto PlayerActor = Cast<APlayerPawn>(OtherActor);
	if (PlayerActor != nullptr)
	{
		InteractionPromptWidget->SetHiddenInGame(true);
		PlayerActor->CurrentlyCollectible = nullptr;
	}
}
