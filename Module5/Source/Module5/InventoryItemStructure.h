// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CollectibleStructure.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "InventoryItemStructure.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FInventoryItemStructure
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Count;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FCollectibleStructure CollectibleInfo;
};
