// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerPawnState.h"
#include "BasePlayerPawnState.generated.h"

/**
 * 
 */
UCLASS()
class MODULE5_API UBasePlayerPawnState : public UPlayerPawnState
{
	GENERATED_BODY()

public:
	virtual ~UBasePlayerPawnState();

	virtual void EnterState(APlayerPawn& pawn) override;
	virtual void ExitState(APlayerPawn& pawn) override;
	virtual UPlayerPawnState* HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent) override;
	virtual UPlayerPawnState* HandleAxis(APlayerPawn& pawn, InputAxisType axisType, float inputValue) override;
	virtual UPlayerPawnState* Update(APlayerPawn& pawn, float dt) override;

protected:
	bool AttemptToGrabWall(APlayerPawn& pawn);
};
