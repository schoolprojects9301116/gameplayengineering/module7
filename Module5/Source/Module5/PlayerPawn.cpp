// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "CollectibleActor.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/Image.h"
#include "Components/ProgressBar.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MinClimbAngle = -75;
	MaxClimbAngle = 75;

	GliderMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Glider Mesh"));
	if (GliderMesh != nullptr)
	{
		GliderMesh->SetHiddenInGame(true);
		GliderMesh->SetupAttachment(GetMesh());
	}
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	if (startingState->GetDefaultObject() != nullptr && startingState->GetDefaultObject()->GetClass() != nullptr)
	{
		currentState = NewObject<UPlayerPawnState>(this, startingState);
	}

	potionWidget = CreateWidget<UPotionWidget>(Cast<APlayerController>(GetController()), potionWidgetType);
	if(potionWidget != nullptr)
	{
		potionWidget->AddToViewport();
	}
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UPlayerPawnState* nextState = currentState->Update(*this, DeltaTime);
	if (nextState != nullptr)
	{
		currentState->ExitState(*this);
		currentState = nextState;
		currentState->EnterState(*this);
	}
	if (PotionDuration > 0)
	{
		PotionDuration -= DeltaTime;
		if (potionWidget != nullptr)
		{
			potionWidget->UpdateProgressBar(PotionDuration, maxPotionDuration);
		}
		if (PotionDuration <= 0)
		{
			if(potionWidget != nullptr)
			{
				potionWidget->ActivePotionImage->SetVisibility(ESlateVisibility::Hidden);
				potionWidget->ActivePotionTimer->SetVisibility(ESlateVisibility::Hidden);
			}
			HasActivePotion = false;
			UndoPotionMods();
		}
	}

}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerPawn::ForwardMoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerPawn::ForwardMoveRight);

	PlayerInputComponent->BindAction<ForwardInputActionDelegate>("Jump", IE_Pressed, this, &APlayerPawn::ForwardInputAction,
		InputActionType::Jump, IE_Pressed);
	PlayerInputComponent->BindAction<ForwardInputActionDelegate>("Jump", IE_Released, this, &APlayerPawn::ForwardInputAction,
		InputActionType::Jump, IE_Released);

	PlayerInputComponent->BindAction<ForwardInputActionDelegate>("Climb", IE_Pressed, this, &APlayerPawn::ForwardInputAction,
		InputActionType::Climb, IE_Pressed);

	PlayerInputComponent->BindAction<ForwardInputActionDelegate>("Attack", IE_Pressed, this, &APlayerPawn::ForwardInputAction,
		InputActionType::Attack, IE_Pressed);

	PlayerInputComponent->BindAction<ForwardInputActionDelegate>("Interact", IE_Pressed, this, &APlayerPawn::ForwardInputAction,
		InputActionType::Interact, IE_Pressed);

	PlayerInputComponent->BindAction<ForwardInputActionDelegate>("Inventory", IE_Pressed, this, &APlayerPawn::ForwardInputAction,
		InputActionType::Inventory, IE_Pressed);

	PlayerInputComponent->BindAction<ForwardInputActionDelegate>("Potion", IE_Pressed, this, &APlayerPawn::ForwardInputAction,
		InputActionType::Potion, IE_Pressed);
}

void APlayerPawn::ForwardInputAction(InputActionType actionType, EInputEvent inputEvent)
{
	if (currentState != nullptr)
	{
		UPlayerPawnState* nextState = currentState->HandleAction(*this, actionType, inputEvent);
		if (nextState != nullptr)
		{
			currentState->ExitState(*this);
			currentState = nextState;
			currentState->EnterState(*this);
		}
	}
}

void APlayerPawn::ForwardMoveForward(float axisValue)
{
	ForwardInputAxis(InputAxisType::MoveForward, axisValue);
}

void APlayerPawn::ForwardMoveRight(float axisValue)
{
	ForwardInputAxis(InputAxisType::MoveRight, axisValue);
}

void APlayerPawn::ForwardInputAxis(InputAxisType axisType, float axisValue)
{
	if (currentState != nullptr)
	{
		UPlayerPawnState* nextState = currentState->HandleAxis(*this, axisType, axisValue);
		if (nextState != nullptr)
		{
			currentState->ExitState(*this);
			currentState = nextState;
			currentState->EnterState(*this);
		}
	}
}

void APlayerPawn::AddToInventory()
{
	if (CurrentlyCollectible != nullptr)
	{
		auto properties = CurrentlyCollectible->CollectibleProperties;
		if (Inventory.Contains(properties.ItemName))
		{
			Inventory[properties.ItemName].Count++;
		}
		else
		{
			FInventoryItemStructure inventoryItem;
			inventoryItem.Count = 1;
			inventoryItem.CollectibleInfo = properties;
			Inventory.Add(properties.ItemName, inventoryItem);
		}

		CurrentlyCollectible->Destroy();
		CurrentlyCollectible = nullptr;
	}
}

TMap<FString, FInventoryItemStructure> APlayerPawn::GetInventory() const
{
	return Inventory;
}

void APlayerPawn::RemoveOneItemFromInventory(FString itemName)
{
	if (Inventory.Contains(itemName))
	{
		auto& item = Inventory[itemName];
		item.Count--;
		if (item.Count == 0)
		{
			Inventory.Remove(itemName);
		}
	}
}

void APlayerPawn::ApplyPotionMods()
{
	auto moveComponent = GetCharacterMovement();
	auto mod = ActivePotion.Modifier + 1;
	if (ActivePotion.ModifierName == "Speed")
	{
		moveComponent->MaxWalkSpeed *= mod;
	}
	else if (ActivePotion.ModifierName == "Jump")
	{
		moveComponent->JumpZVelocity *= mod;
	}
	else if (ActivePotion.ModifierName == "Climb")
	{
		moveComponent->MaxFlySpeed *= mod;
	}
}

void APlayerPawn::UndoPotionMods()
{
	auto moveComponent = GetCharacterMovement();
	auto mod = ActivePotion.Modifier + 1;
	if (ActivePotion.ModifierName == "Speed")
	{
		moveComponent->MaxWalkSpeed /= mod;
	}
	else if (ActivePotion.ModifierName == "Jump")
	{
		moveComponent->JumpZVelocity /= mod;
	}
	else if (ActivePotion.ModifierName == "Climb")
	{
		moveComponent->MaxFlySpeed /= mod;
	}
}

void APlayerPawn::UsePotion()
{
	if (HasPotion)
	{
		HasPotion = false;
		if (HasActivePotion)
		{
			UndoPotionMods();
		}
		ActivePotion = CurrentPotion;
		if(potionWidget != nullptr)
		{
			potionWidget->HeldPotionImage->SetVisibility(ESlateVisibility::Hidden);
			potionWidget->ActivePotionImage->SetVisibility(ESlateVisibility::Visible);
			potionWidget->ActivePotionTimer->SetVisibility(ESlateVisibility::Visible);
			potionWidget->ActivePotionImage->SetBrushFromTexture(ActivePotion.PotionImage);
		}
		HasActivePotion = true;
		
		ApplyPotionMods();

		PotionDuration = maxPotionDuration;
	}
}

void APlayerPawn::AddPotion(FPotionStructure potion)
{
	HasPotion = true;
	CurrentPotion = potion;
	if(potionWidget != nullptr)
	{
		potionWidget->HeldPotionImage->SetVisibility(ESlateVisibility::Visible);
		potionWidget->HeldPotionImage->SetBrushFromTexture(CurrentPotion.PotionImage);
	}
}
