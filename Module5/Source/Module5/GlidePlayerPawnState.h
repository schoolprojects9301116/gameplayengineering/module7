// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "JumpPlayerPawnState.h"
#include "GlidePlayerPawnState.generated.h"

/**
 * 
 */
UCLASS()
class MODULE5_API UGlidePlayerPawnState : public UJumpPlayerPawnState
{
	GENERATED_BODY()
public:
	virtual ~UGlidePlayerPawnState();

	virtual void EnterState(APlayerPawn& pawn);
	virtual void ExitState(APlayerPawn& pawn);
	virtual UPlayerPawnState* HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent) override;
	virtual UPlayerPawnState* Update(APlayerPawn& pawn, float dt) override;
};
