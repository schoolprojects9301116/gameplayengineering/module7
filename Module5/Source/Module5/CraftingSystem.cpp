// Fill out your copyright notice in the Description page of Project Settings.


#include "CraftingSystem.h"

FPotionStructure UCraftingSystem::CreatePotion(TArray<FCollectibleStructure> collectibleInfo)
{
	TMap<FString, int> modCount;
	for (auto& item : collectibleInfo)
	{
		for (auto& mod : item.PropertyModifiers)
		{
			if (!PossibleMods.Contains(mod.Key))
			{
				continue;
			}
			if (modCount.Contains(mod.Key))
			{
				modCount[mod.Key]++;
			}
			else
			{
				modCount.Add(mod.Key, 1);
			}
		}
	}
	bool ModApplied = false;
	TPair<FString, int> ModToMake;
	for (auto& mod : modCount)
	{
		if (!ModApplied && mod.Value > 1)
		{
			ModToMake = mod;
			ModApplied = true;
		}
		else if (mod.Value > ModToMake.Value)
		{
			ModToMake = mod;
		}
	}

	if (ModApplied)
	{
		FPotionStructure potion;
		potion.ModifierName = ModToMake.Key;
		potion.PotionName = ModToMake.Key + "Potion";
		potion.Modifier = 0;
		potion.PotionImage = PossibleMods[ModToMake.Key];
		for (auto& item : collectibleInfo)
		{
			if (item.PropertyModifiers.Contains(ModToMake.Key))
			{
				potion.Modifier += item.PropertyModifiers[ModToMake.Key];
			}
		}
		return potion;
	}
	else
	{
		return RuinedPotion;
	}
}