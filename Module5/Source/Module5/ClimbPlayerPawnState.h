// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerPawnState.h"
#include "ClimbPlayerPawnState.generated.h"

/**
 * 
 */
UCLASS()
class MODULE5_API UClimbPlayerPawnState : public UPlayerPawnState
{
	GENERATED_BODY()

public:
	virtual ~UClimbPlayerPawnState();
	virtual UPlayerPawnState* HandleAxis(APlayerPawn& pawn, InputAxisType axisType, float inputValue) override;
	virtual UPlayerPawnState* HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent) override;

protected:
	bool ClimbMovement(APlayerPawn& pawn, FVector direction, float inputValud);
	void DropFromWall(APlayerPawn& pawn);
};
