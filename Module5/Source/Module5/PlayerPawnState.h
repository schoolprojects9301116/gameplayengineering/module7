// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PlayerPawnState.generated.h"

class APlayerPawn;

UENUM()
enum class InputActionType
{
	Jump,
	Attack,
	Climb,
	Interact,
	Inventory,
	Potion
};

UENUM()
enum class InputAxisType
{
	MoveForward,
	MoveRight
};

/**
 * 
 */
UCLASS(Abstract)
class MODULE5_API UPlayerPawnState : public UObject
{
	GENERATED_BODY()

public:
	UPlayerPawnState();
	virtual ~UPlayerPawnState();

	virtual void EnterState(APlayerPawn& pawn);
	virtual void ExitState(APlayerPawn& pawn);
	virtual UPlayerPawnState* HandleAction(APlayerPawn& pawn, InputActionType actionType, EInputEvent inputEvent);
	virtual UPlayerPawnState* HandleAxis(APlayerPawn& pawn, InputAxisType axisType, float inputValue);
	virtual UPlayerPawnState* Update(APlayerPawn& pawn, float dt);
};
