// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InventoryItemStructure.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InventoryItemIcon.generated.h"

class UTextBlock;
class UImage;
class UTexture2D;
class UButton;

UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInventoryItemSelectedSignature, FInventoryItemStructure, InventoryItem);

/**
 * 
 */
UCLASS()
class MODULE5_API UInventoryItemIcon : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable)
	void SetCount(int count);

	UFUNCTION(BlueprintCallable)
	void SetImage(UTexture2D * img);

	UFUNCTION(BlueprintCallable)
	void SetupItemIcon(FInventoryItemStructure itemStruct);

	UFUNCTION()
	void ResetIcon();

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton * ItemButton;

	UPROPERTY()
	FInventoryItemSelectedSignature ItemSelected;

protected:
	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UTextBlock * ItemCount;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage * ItemImage;

	UPROPERTY()
	FInventoryItemStructure ItemStructure;

	UFUNCTION()
	void ButtonPressed();

	UPROPERTY()
	int currentItemCount;
};
